const fs = require('fs')
const parse = require('csv-parse')
const sgMail = require('@sendgrid/mail')
const arg = require('arg')

const key = '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'
sgMail.setApiKey(key)

const args = arg({
  '--contact-file-path': String,
  '--template-id': String,
  '--subject': String
})

if (args['--contact-file-path'] && args['--template-id']) {
  fs.createReadStream(args['--contact-file-path'])
  .pipe(parse({ 
    from: 2,
    delimiter: ','
  }))
    .on('data', async (data) => {
      console.log('sending email to ' + data[1]);
      const msg = {
        to: data[1],
        from: 'olympiad@balatech.asia',
        subject: args['--subject'],
        "template_id": args['--template-id'],
        "dynamic_template_data": {
          "firstName": data[2],
          "points": data[4]
        }
      }
      try {
        const ready = await sgMail.send(msg)
        if (ready.ok)
        console.log('sent email to ' + data[1]);
      } catch (e) {
        console.error(`email ${data[1]} is not real...`)
      }
    })
} else {
  console.log('how to use properly: specify -t and --contact-file-path like:');
  console.log('node index.js --template-id=d-9d09d207a0584bc095fe97ee32c50464 --contact-file-path=moreThanTen.csv');
}


