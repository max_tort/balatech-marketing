require('dotenv').config()
const mongoose = require('mongoose')
const arg = require('arg')
const fs = require('fs')
const parse = require('csv-parse')
const MongoClient = require('mongodb').MongoClient;
const { exit } = require('process')

const args = arg({
  '--input-file': String,
  '--output-file': String,
  // '-i': '--input-file',
  // '-o': '--output-file'
})

let usercount = 0
  let errorcount = 0

const uri = process.env.MONGO_URI
const dbName = process.env.DB_NAME

MongoClient.connect(uri, async (err, client) => {
  if (err) {
    console.error(err);
    exit()
  }
  const problemsDB = client.db(dbName)

  const users = problemsDB.collection('users')

  if (args['--input-file'] && args['--output-file']) {
    const writeStream = fs.createWriteStream(args['--output-file'], { flags: 'a' })
    writeStream.write(`email;country;locality;school\r\n`)
    fs.createReadStream(args['--input-file'])
      .pipe(
        parse({
          from: 2,
          delimiter: ','
        })
      )
      .on('data', async data => {
        const email = data[1]
        try {
          // console.log('trying to find ' + email);
          const user = await users.findOne({ email: { '$regex': new RegExp(email, 'i') }})
          // console.log(user.country);
          writeStream.write(`${email};${user.country || 'none'};${user.locality || 'none'};${user.school || 'none'}\r\n`)
          console.log(`successfully wrote user #${usercount}`);
          usercount++
        }
        catch (err) {
          console.log(`user ${email} doesn't exist`);
          console.log(err);
          errorcount++
        }
      })
  } else {
    console.error('you have to specify --input-file and --output-file')
    exit()
  }

  console.log(`writeCount: ${usercount}`);
  console.log(`errorCount: ${errorcount}`);

  // client.close();
})


